import 'package:flutter/material.dart';
import 'package:clipboard_manager/clipboard_manager.dart';
import 'package:android_intent/android_intent.dart';
import 'package:platform/platform.dart';
import 'package:address_lookup/geocoding.dart';

class Matches extends StatelessWidget {
  final List<AddressMatch> items;
  final Platform platform = LocalPlatform();

  Matches(this.items);

  Widget empty(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text("Nothing to see here"),
        Text("Please try again"),
      ],
    );
  }

  Widget buildItem(BuildContext context, int index) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.location_on),
            title: Text(items[index].matchedAddress),
            subtitle: Text("Latitude: ${items[index].coordinates.x}, Longitude: ${items[index].coordinates.y}"),
          ),
          ButtonTheme.bar(
            child: ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: const Text('COPY'),
                  onPressed: () {
                    ClipboardManager
                        .copyToClipBoard("${items[index].coordinates.y},${items[index].coordinates.x}")
                        .then((result) {
                          final snackBar = SnackBar(
                            content: Text('Coordinates copied to clipboard'),
                            action: SnackBarAction(label: 'Undo', onPressed: () {}),
                          );

                          Scaffold.of(context).showSnackBar(snackBar);
                    });
                  },
                ),
                FlatButton(
                  child: const Text('MAP'),
                  onPressed: () {
                    if (platform.isAndroid) {
                      final AndroidIntent intent = AndroidIntent(
                        action: 'action_view',
                        data: Uri.encodeFull('geo:${items[index].coordinates.y},${items[index].coordinates.x}'),
                      );
                      intent.launch();
                    }
                  },
                ),
              ],
            )
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (items.length == 0) {
      return empty(context);
    }

    return ListView.builder(
      itemBuilder: buildItem,
      itemCount: items.length,
    );
  }
}