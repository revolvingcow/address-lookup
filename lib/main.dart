import 'package:flutter/material.dart';
import 'package:address_lookup/geocoding.dart';
import 'package:address_lookup/matches.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Address Lookup',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Address Lookup'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<AddressMatch> matches = [];
  final controller = TextEditingController();

  void performLookup() {
    Geocoding().lookup(controller.text)
        .then((value) => setState(() {
          matches.clear();
          matches.addAll(value.addressMatches);
        }))
        .catchError((error) => print(error.toString()));
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(10.0),
            child: TextFormField(
              controller: controller,
              decoration: InputDecoration(
                  labelText: 'Address'
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter an address';
                }
                return null;
              },
            ),
          ),
          Expanded(
            child: Matches(matches),
          )
        ]
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: performLookup,
        tooltip: 'Search',
        child: Icon(Icons.search),
      ),
    );
  }
}
