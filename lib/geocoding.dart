import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class CensusResponse {
  Benchmark benchmark;
  Address address;
  List<AddressMatch> addressMatches;

  CensusResponse.map(dynamic obj) {
    dynamic result = obj["result"];

    dynamic input = result["input"];
    this.benchmark = Benchmark.map(input["benchmark"]);
    this.address = Address.map(input["address"]);

    this.addressMatches = result["addressMatches"].map<AddressMatch>((m) => AddressMatch.map(m)).toList();
  }
}

class Benchmark {
  String id;
  String benchmarkName;
  String benchmarkDescription;
  bool isDefault;

  Benchmark.map(dynamic obj) {
    this.id = obj["id"];
    this.benchmarkName = obj["benchmarkName"];
    this.benchmarkDescription = obj["benchmarkDescription"];
    this.isDefault = obj["isDefault"];
  }
}

class Address {
  String address;

  Address.map(dynamic obj) {
    this.address = obj["address"];
  }
}

class AddressMatch {
  String matchedAddress;
  Coordinates coordinates;
  TigerLine tigerLine;
  AddressComponents addressComponents;

  AddressMatch.map(dynamic obj) {
    this.matchedAddress = obj["matchedAddress"];
    this.coordinates = Coordinates.map(obj["coordinates"]);
    this.tigerLine = TigerLine.map(obj["tigerLine"]);
    this.addressComponents = AddressComponents.map(obj["addressComponents"]);
  }
}

class Coordinates {
  double x;
  double y;

  Coordinates.map(dynamic obj) {
    this.x = obj["x"];
    this.y = obj["y"];
  }
}

class TigerLine {
  String tigerLineId;
  String side;

  TigerLine.map(dynamic obj) {
    this.tigerLineId = obj["tigerLineId"];
    this.side = obj["side"];
  }
}

class AddressComponents {
  String fromAddress;
  String toAddress;
  String preQualifier;
  String preDirection;
  String preType;
  String streetName;
  String suffixType;
  String suffixDirection;
  String suffixQualifier;
  String city;
  String state;
  String zip;

  AddressComponents.map(dynamic obj) {
    this.fromAddress = obj["fromAddress"];
    this.toAddress = obj["toAddress"];
    this.preQualifier = obj["preQualifier"];
    this.preDirection = obj["preDirection"];
    this.preType = obj["preType"];
    this.streetName = obj["streetName"];
    this.suffixType = obj["suffixType"];
    this.suffixDirection = obj["suffixDirection"];
    this.city = obj["city"];
    this.state = obj["state"];
    this.zip = obj["zip"];
  }
}

class Geocoding {
  final JsonDecoder decoder = new JsonDecoder();
  final String endpoint = "https://geocoding.geo.census.gov/geocoder";
  final String returnType = "locations"; // locations, geographies
  final String searchType = "onelineaddress"; // address, onelineaddress, coordinates
  final String benchmark = "Public_AR_Current"; // Public_AR_Current
  final String format = "json"; // format (json, html), layers

  Future<dynamic> get(String url) {
    return http.get(url).then((http.Response response) {
      final String body = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }

      return decoder.convert(body);
    });
  }

  Future<dynamic> post(String url, {Map headers, body, encoding}) {
    return http.post(url, body: body, headers: headers, encoding: encoding).then((http.Response response) {
      final String body = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }

      return decoder.convert(body);
    });
  }

  Future<CensusResponse> lookup(String address) async {
    dynamic json = await get("$endpoint/$returnType/onelineaddress?address=$address&benchmark=$benchmark&format=$format");
    CensusResponse censusResponse = CensusResponse.map(json);
    return censusResponse;
  }

  Future<CensusResponse> lookupAddress(String street, String city, String state, String zip) {
    return lookup("$street, $city, $state $zip");
  }
}